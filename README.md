# Pokemon Android Engine
A game engine aiming to recreate the 2nd generation of Pokemon games on Android.

Zach Harsh, Justin Kuzma

This was originally developed for a Mobile App Development course in college. 
It is inspired by the second generation of Pokemon games as those will always be my personal favorites in the series (Fact: There is no better Pokemon game than Pokemon Silver).

This Engine is built on top of the e3roid OpenGL Engine for Android allowing for hardware acceleration and easier graphics handling in the code. It is a long way from completion and will likely never truly be completed, but it is 100% open source so that anyone can use it if they want to.

As far as current completion, it is far more advanced than my Java Engine was in terms of functionality and many improvements have been made such as authentic damage calculation, proper Pokemon stat generation, and much more. It also utilizes resources closely resembling (and in some cases, 100% identical to) the format used in the popular Pokemon Essentials starter kit for RMXP.
